﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace paint {
	public class BrushTool : ChunkTool { 
		private Image icon;

		public override string Name => "Simple Brush";
		public override Image Icon => icon;

		private Pen pen;
		private int penWidth = 5;

		public BrushTool (MainWindow window) : base (window) {
			icon = Properties.Resources.BrushIcon;
		}

		public override bool OnUndo (PictureBox canvas) {
			return !IsDrawing;
		}

		public override void OnSelected (FlowLayoutPanel toolPanel) {
			base.OnSelected (toolPanel);
		}

		public override void OnDeselected (FlowLayoutPanel toolPanel) {
			base.OnDeselected (toolPanel);
		}

		public override void OnMouseDown (MouseEventArgs e, PictureBox canvas, Point position) {
			pen = new Pen (MainWindow.PrimaryColor, penWidth);
			pen.EndCap = LineCap.Round;

			startDrawing (canvas);
			base.OnMouseDown (e, canvas, position);
		}

		private void markChunks (Point start, Point end) {
			const int discreteness = 10;

			Point vector = new Point (end.X - start.X, end.Y - start.Y);
			double length = Math.Sqrt (vector.X * vector.X + vector.Y * vector.Y);
			double dx = vector.X / length, dy = vector.Y / length;

			int steps = (int) Math.Ceiling (length / discreteness) + 1;
			int r = (int) Math.Ceiling ((double) penWidth / 2);

			for (int i = 0; i < steps; ++i) {
				double s = Math.Min (i * discreteness, length);
				Point c = new Point ((int) Math.Floor (start.X + dx * s), (int) Math.Floor (start.Y + dy * s));

				for (int x = c.X - 2 * r; x <= c.X + 2 * r; x = x + r)
					for (int y = c.Y - 2 * r; y <= c.Y + 2 * r; y = y + r) markChunk (x, y);
			}
		}

		public override void OnMouseMove (MouseEventArgs e, PictureBox canvas, Point lastPosition, Point position, bool isMouseDown) {
			if (isMouseDown) {
				Graphics context = Graphics.FromImage (canvas.Image);

				context.SmoothingMode = SmoothingMode.HighSpeed;
				context.DrawLine (pen, lastPosition, position);

				markChunks (lastPosition, position);

				Rectangle redrawArea = new Rectangle (
					Math.Min (lastPosition.X, position.X) - penWidth, Math.Min (lastPosition.Y, position.Y) - penWidth,
					Math.Abs (position.X - lastPosition.X) + 2 * penWidth,
					Math.Abs (position.Y - lastPosition.Y) + 2 * penWidth);

				canvas.Invalidate (redrawArea);
			}

			base.OnMouseMove (e, canvas, lastPosition, position, isMouseDown);
		}

		public override void OnMouseUp (MouseEventArgs e, PictureBox canvas, Point position) {
			endDrawing (canvas);
			base.OnMouseUp (e, canvas, position);
		}
	}
}
