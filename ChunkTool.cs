﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Diagnostics;

namespace paint {
	public class ChunkToolAction : IHistoryAction {
		private struct ChunkData {
			public int x, y;
			public Bitmap data, modified;

			public ChunkData (int x, int y, Bitmap image, Bitmap modified) {
				this.x = x;
				this.y = y;
				data = image;
				this.modified = modified;
			}
		}

		private List<ChunkData> data = new List<ChunkData> ();
		private int pixelsStored;
		public int cost => pixelsStored;

		public ChunkToolAction (Image prevImage, Image image, int chunkWidth, bool [] input) {
			pixelsStored = 0;
			int chunkSize = chunkWidth * chunkWidth;

			int chunksX = (int) Math.Ceiling ((double) image.Width / chunkWidth);
			int chunksY = (int) Math.Ceiling ((double) image.Height / chunkWidth);

			for (int i = 0; i < input.Length && i < chunksX * chunksY; ++i) {
				if (input [i]) {
					int y = i / chunksX, x = i % chunksX;

					int sourceWidth = Math.Min (chunkWidth, image.Width - x * chunkWidth);
					int sourceHeight = Math.Min (chunkWidth, image.Height - y * chunkWidth);

					Rectangle sourceRect = new Rectangle (x * chunkWidth, y * chunkWidth, sourceWidth, sourceHeight);
					PixelFormat format = image.PixelFormat;

					Bitmap bmp = (prevImage as Bitmap).Clone (sourceRect, format);
					Bitmap modified = (image as Bitmap).Clone (sourceRect, format);

					data.Add (new ChunkData (x * chunkWidth, y * chunkWidth, bmp, modified));
					pixelsStored = pixelsStored + chunkSize;
				}
			}
		}

		public void redo (Image image) {
			Graphics context = Graphics.FromImage (image);
			foreach (ChunkData chunk in data) {
				context.DrawImage (chunk.modified, chunk.x, chunk.y, chunk.modified.Width, chunk.modified.Height);
			}
		}

		public void undo (Image image) {
			Graphics context = Graphics.FromImage (image);
			foreach (ChunkData chunk in data) {
				context.DrawImage (chunk.data, chunk.x, chunk.y, chunk.data.Width, chunk.data.Height);
			}
		}

		void IDisposable.Dispose () {
			foreach (ChunkData chunk in data) {
				chunk.data.Dispose ();
			}
		}
	}

	public abstract class ChunkTool : Tool {
		private PictureBox initialCanvas;
		private Image prevImage;
		private int chunksX, chunksY;
		private bool [] chunks;
		private bool isDrawing;

		public bool IsDrawing {
			get { return isDrawing; }
		}

		public const int chunkWidth = 32;

		public ChunkTool (MainWindow window) : base (window) { }

		protected void startDrawing (PictureBox canvas) {
			MainWindow.HistoryManager.LockRedo = true;

			chunksX = (int) Math.Ceiling ((double) canvas.Image.Width / chunkWidth);
			chunksY = (int) Math.Ceiling ((double) canvas.Image.Height / chunkWidth);

			chunks = new bool [chunksX * chunksY];
			for (int i = 0; i < chunksX * chunksY; ++i) chunks [i] = false;

			prevImage = canvas.Image.Clone () as Image;

			initialCanvas = canvas;
			isDrawing = true;
		}

		protected void markChunk (int x, int y) {
			int cx = x / chunkWidth, cy = y / chunkWidth;

			int index = cy * chunksX + cx;
			if (index >= 0 && index < chunks.Length) chunks [index] = true;
		}

		protected void cancelDrawing () {
			if (prevImage != null) {
				prevImage.Dispose ();
				prevImage = null;
			}

			MainWindow.HistoryManager.LockRedo = false;
		}

		protected void endDrawing () {
			endDrawing (initialCanvas);
		}

		protected void endDrawing (PictureBox canvas) {
			if (isDrawing) {
				int chunkc = chunksX * chunksY;
				int chunkm = 0;
				for (int i = 0; i < chunkc; ++i) if (chunks [i]) chunkm++;

				ChunkToolAction action = new ChunkToolAction (prevImage, canvas.Image, chunkWidth, chunks);
				MainWindow.HistoryManager.saveAction (action);

				if (prevImage != null) {
					prevImage.Dispose ();
					prevImage = null;
				}

				isDrawing = false;
				MainWindow.HistoryManager.LockRedo = false;
			}
		}
	}
}
