﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace paint {
	public class ColorPicker : UserControl {
		private static int [] ColorPalette = { 
			0x000000, 0xFFFFFF, 0x7F7F7F, 0xC3C3C3,
			0x880015, 0xB97A57, 0xED1C24, 0xFFAEC9,
			0xFF7F27, 0xFFC90E, 0xFFF200, 0xEFE4B0,
			0x22B14C, 0xB5E61D, 0x00A2E8, 0x99D9EA,
			0x3F48CC, 0x7092BE, 0xA349A4, 0xC8BFE7 
		};

		private class ColorPickerButton : Button {
			private Color color = Color.Blue;
			private bool isChecked = false;

			public Color Color {
				get { return color; }
				set {
					color = value;
					Invalidate ();
				}
			}

			public bool Checked {
				get {
					return isChecked;
				}

				set {
					isChecked = value;

					if (isChecked) {
						BackColor = SystemColors.GradientInactiveCaption;
					} else {
						BackColor = SystemColors.Control;
					}
				}
			}

			public ColorPickerButton (Color color) {
				SetStyle (ControlStyles.Selectable, false);
				BackColor = SystemColors.Control;

				FlatStyle = FlatStyle.Flat;
				TextAlign = ContentAlignment.BottomCenter;

				Font = new Font (Font.FontFamily, 8);

				this.color = color;
			}

			protected override void OnClick (EventArgs e) {
				base.OnClick (e);
				Checked = true;
			}

			protected override void OnPaint (PaintEventArgs e) {
				if (Checked) {
					FlatAppearance.BorderColor = SystemColors.MenuHighlight;
					FlatAppearance.MouseOverBackColor = SystemColors.GradientActiveCaption;
				} else {
					FlatAppearance.BorderColor = SystemColors.Control;
					FlatAppearance.MouseOverBackColor = SystemColors.GradientInactiveCaption;
				}

				base.OnPaint (e);

				const int colorWidth = 20;

				Rectangle colorDisplayRect = new Rectangle (ClientRectangle.X + ((ClientRectangle.Width - colorWidth) / 2), 
					ClientRectangle.Y + 10, colorWidth, colorWidth);

				Brush displayBrush = new SolidBrush (color);
				e.Graphics.FillRectangle (displayBrush, colorDisplayRect);

				Pen pen = new Pen (Color.Gray, 1), penOutline = new Pen (Color.White, 1);
				e.Graphics.DrawRectangle (pen, colorDisplayRect);
				e.Graphics.DrawRectangle (penOutline, new Rectangle (colorDisplayRect.X + 1, colorDisplayRect.Y + 1,
					colorDisplayRect.Width - 2, colorDisplayRect.Height - 2));
			}
		}

		private class ColorPickerAdvanced : Button {
			private Image icon;

			public ColorPickerAdvanced () {
				SetStyle (ControlStyles.Selectable, false);
				BackColor = SystemColors.Control;

				FlatStyle = FlatStyle.Flat;
				TextAlign = ContentAlignment.BottomCenter;

				Font = new Font (Font.FontFamily, 8);
				icon = Properties.Resources.RgbChart;
			}

			protected override void OnPaint (PaintEventArgs e) {
				FlatAppearance.BorderColor = SystemColors.Control;
				FlatAppearance.MouseOverBackColor = SystemColors.GradientInactiveCaption;

				base.OnPaint (e);

				const int colorWidth = 20;

				Rectangle colorDisplayRect = new Rectangle (ClientRectangle.X + ((ClientRectangle.Width - colorWidth) / 2),
					ClientRectangle.Y + 10, colorWidth, colorWidth);

				e.Graphics.DrawImage (icon, colorDisplayRect);

				Pen pen = new Pen (Color.Gray, 1), penOutline = new Pen (Color.White, 1);
				e.Graphics.DrawRectangle (pen, colorDisplayRect);
				e.Graphics.DrawRectangle (penOutline, new Rectangle (colorDisplayRect.X + 1, colorDisplayRect.Y + 1,
					colorDisplayRect.Width - 2, colorDisplayRect.Height - 2));
			}
		}

		private class ColorPickerSquare : Button {
			private Color color = Color.Blue;
			private bool hover = false;

			public Color Color {
				get { return color; }
				set { 
					color = value;
					Invalidate ();
				}
			}

			public ColorPickerSquare (Color color) {
				SetStyle (ControlStyles.Selectable, false);
				FlatStyle = FlatStyle.Flat;
				FlatAppearance.BorderColor = SystemColors.Control;
				BackColor = SystemColors.Control;

				this.color = color;
			}

			protected override void OnMouseEnter (EventArgs e) {
				hover = true;
				base.OnMouseEnter (e);
			}

			protected override void OnMouseLeave (EventArgs e) {
				hover = false;
				base.OnMouseLeave (e);
			}

			protected override void OnPaint (PaintEventArgs e) {
				base.OnPaint (e);
				Rectangle drawingRect = new Rectangle (ClientRectangle.X, ClientRectangle.Y, ClientRectangle.Width - 1, ClientRectangle.Height - 1);

				Brush brush = new SolidBrush (color);
				e.Graphics.FillRectangle (brush, drawingRect);

				Pen penOutline = (hover) ? new Pen (SystemColors.GradientInactiveCaption) : new Pen (Color.White, 1);
				Pen pen = (hover) ? new Pen (SystemColors.MenuHighlight) : new Pen (Color.Gray, 1);

				e.Graphics.DrawRectangle (pen, drawingRect);
				e.Graphics.DrawRectangle (penOutline, new Rectangle (drawingRect.X + 1, drawingRect.Y + 1,
					drawingRect.Width - 2, drawingRect.Height - 2));
			}
		}

		private ColorPickerButton primaryColorButton;
		private ColorPickerButton secondaryColorButton;
		private ColorPickerSquare [] colorButtons;
		private ColorPickerAdvanced customColorButton;

		public Color PrimaryColor {
			get { return primaryColorButton.Color; }
			set { if (value != null) primaryColorButton.Color = value; }
		}

		public Color SecondaryColor {
			get { return secondaryColorButton.Color; }
			set { if (value != null) secondaryColorButton.Color = value; }
		}

		public ColorPicker () {
			InitializeComponents ();
		}

		private void colorButton_Click (object sender, EventArgs e) {
			if ((sender as ColorPickerButton) == primaryColorButton) secondaryColorButton.Checked = false;
			else if ((sender as ColorPickerButton) == secondaryColorButton) primaryColorButton.Checked = false;
		}

		private void colorSelection_Click (object sender, EventArgs e) {
			ColorPickerSquare button = sender as ColorPickerSquare;
			
			if (button != null) {
				if (primaryColorButton.Checked) primaryColorButton.Color = button.Color;
				else secondaryColorButton.Color = button.Color;
			}
		}

		private void customColorButton_Click (object sender, EventArgs e) {
			using (ColorDialog dialog = new ColorDialog ()) {
				if (primaryColorButton.Checked) dialog.Color = primaryColorButton.Color;
				else dialog.Color = secondaryColorButton.Color;

				if (dialog.ShowDialog () == DialogResult.OK) {
					if (primaryColorButton.Checked) primaryColorButton.Color = dialog.Color;
					else secondaryColorButton.Color = dialog.Color;
				}
			}
		}

		private void InitializeComponents () {
			primaryColorButton = new ColorPickerButton (Color.Black);
			secondaryColorButton = new ColorPickerButton (Color.White);

			primaryColorButton.Text = "Color 1";
			secondaryColorButton.Text = "Color 2";

			primaryColorButton.Height = 65;
			secondaryColorButton.Height = 65;

			primaryColorButton.Width = 48;
			secondaryColorButton.Width = 48;

			secondaryColorButton.Location = new Point (50, 0);

			primaryColorButton.Checked = true;

			primaryColorButton.Click += colorButton_Click;
			secondaryColorButton.Click += colorButton_Click;

			Controls.Add (primaryColorButton);
			Controls.Add (secondaryColorButton);

			// initialize the color buttons
			colorButtons = new ColorPickerSquare [ColorPalette.Length];

			for (int i = 0; i < ColorPalette.Length; ++i) {
				ColorPickerSquare button = new ColorPickerSquare (Color.FromArgb (
					(ColorPalette [i] >> 16) & 0xff,
					(ColorPalette [i] >> 8) & 0xff,
					(ColorPalette [i]) & 0xff
				));

				button.Location = new Point (110 + (i / 2) * 23, 8 + (i % 2) * 23);
				button.Width = button.Height = 21;

				button.Click += colorSelection_Click;

				Controls.Add (button);
				colorButtons [i] = button;
			}

			// custom color picker
			customColorButton = new ColorPickerAdvanced ();

			customColorButton.Text = "More Colors";
			customColorButton.Height = 65;
			customColorButton.Width = 50;
			customColorButton.Location = new Point (345, 0);
			customColorButton.Click += customColorButton_Click;

			Controls.Add (customColorButton);
		}
	}
}
