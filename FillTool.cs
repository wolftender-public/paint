﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing.Imaging;

namespace paint {
	public class FillTool : ChunkTool {
		private Image icon;

		public override string Name => "Fill";
		public override Image Icon => icon;

		private int maxDistance = 0;

		public FillTool (MainWindow window) : base (window) {
			icon = Properties.Resources.FillIcon;
		}

		private int toBGRA (Color color) {
			return color.B + (color.G << 8) + (color.R << 16) + (color.A << 24);
		}

		private int coordsToIndex (int x, int y, int stride) {
			return (stride * y) + (x * 4);
		}

		private unsafe bool checkPixel (byte* pixel, byte* color) {
			return (
				Math.Abs (pixel [0] - color [0]) <= maxDistance &&
				Math.Abs (pixel [1] - color [1]) <= maxDistance &&
				Math.Abs (pixel [2] - color [2]) <= maxDistance
			);
		}

		private bool compareRGB (Color a, Color b) {
			return (a.R == b.R && a.G == b.G && a.B == b.B);
		}

		// TODO: optimize this even more so that the fill is even faster
		private unsafe void scan (int lx, int rx, int y, ref Point [] stack, ref int stackSize, byte* scan0, int stride, int width, byte* targetColor) {
			bool added = false;
			for (int x = lx; x < rx; ++x) {
				if (x >= width - 1 || !checkPixel ((scan0 + coordsToIndex (x, y, stride)), targetColor)) added = false;
				else if (!added) {
					stack [stackSize++] = new Point (x, y);
					added = true;
				}
			}
		}

		public override void OnMouseClick (MouseEventArgs e, PictureBox canvas, Point position) {
			Bitmap bitmap = (canvas.Image as Bitmap);
			if (bitmap == null) return;

			if (compareRGB (bitmap.GetPixel (position.X, position.Y), MainWindow.PrimaryColor)) {
				base.OnMouseClick (e, canvas, position);
				return;
			}

			startDrawing (canvas);

			int fillColor = toBGRA (MainWindow.PrimaryColor);
			BitmapData bmData = bitmap.LockBits (new Rectangle (0, 0, bitmap.Width, bitmap.Height),
				ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

			Point [] stack = new Point [bitmap.Width * bitmap.Height + 1];
			int stackSize = 0;

			unsafe {
				byte* scan0 = (byte*) (void*) bmData.Scan0;
				int startLocation = coordsToIndex (position.X, position.Y, bmData.Stride);
				int targetColor = *((int*) (scan0 + startLocation));

				stack [stackSize++] = position;
				while (stackSize > 0) {
					Point current = stack [--stackSize];
					int lx = current.X, x = current.X;

					while (lx > 0 && checkPixel ((scan0 + coordsToIndex (lx - 1, current.Y, bmData.Stride)), (byte*) &targetColor)) {
						((int*) (scan0 + coordsToIndex (lx - 1, current.Y, bmData.Stride))) [0] = fillColor;
						markChunk (lx - 1, current.Y);
						lx = lx - 1;
					}

					while (x <= bitmap.Width - 1 && checkPixel ((scan0 + coordsToIndex (x, current.Y, bmData.Stride)), (byte*) &targetColor)) {
						((int*) (scan0 + coordsToIndex (x, current.Y, bmData.Stride))) [0] = fillColor;
						markChunk (x, current.Y);
						x = x + 1;
					}

					if (current.Y < bitmap.Height - 1) scan (lx, x - 1, current.Y + 1, ref stack, ref stackSize, scan0, bmData.Stride, bitmap.Width, (byte*) &targetColor);
					if (current.Y > 0) scan (lx, x - 1, current.Y - 1, ref stack, ref stackSize, scan0, bmData.Stride, bitmap.Width, (byte*) &targetColor);
				}
			}

			bitmap.UnlockBits (bmData);
			endDrawing (canvas);

			canvas.Invalidate ();
			base.OnMouseClick (e, canvas, position);
		}
	}
}