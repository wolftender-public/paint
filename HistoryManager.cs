﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace paint {
	public interface IHistoryAction : IDisposable {
		public int cost { get; }
		public void undo (Image image);
		public void redo (Image image);
	}

	public class HistoryManager {
		private Image image;
		private LinkedList<IHistoryAction> undoStack = new LinkedList<IHistoryAction> ();
		private LinkedList<IHistoryAction> redoStack = new LinkedList<IHistoryAction> ();

		private int undoStackUsage = 0;
		private int redoStackUsage = 0;

		private const int maxUndoStackUsage = 30000000;
		private const int maxRedoStackUsage = 20000000;

		public bool IsChangedSinceSave { get; set; }

		public int UndoStackUsage {
			get { return undoStackUsage; }
		}

		public int RedoStackUsage {
			get { return redoStackUsage; }
		}

		public int UndoStackSize {
			get { return undoStack.Count; }
		}

		public bool LockRedo { get; set; }

		public int RedoStackSize {
			get {
				if (LockRedo) return 0;
				else return redoStack.Count;
			}
		}

		public HistoryManager (Image img) {
			image = img;
			LockRedo = false;
		}

		private void cleanupStacks () {
			if (undoStackUsage > maxUndoStackUsage) {
				IHistoryAction action = undoStack.Last.Value;
				undoStackUsage -= action.cost;
				action.Dispose ();

				undoStack.RemoveLast ();
			}

			if (redoStackUsage > maxRedoStackUsage) {
				IHistoryAction action = redoStack.Last.Value;
				redoStackUsage -= action.cost;
				action.Dispose ();

				redoStack.RemoveLast ();
			}

			// Debug.WriteLine ("UNDO STACK (" + UndoStackSize + ")" + undoStackUsage + "/" + maxUndoStackUsage + ", REDO STACK (" + RedoStackSize + ") " + redoStackUsage + "/" + maxRedoStackUsage);
		}

		public void saveAction (IHistoryAction action) {
			//undoStack.Push (action);
			undoStack.AddFirst (action);
			undoStackUsage += action.cost;

			/*while (redoStack.Count > 0) {
				redoStack.Pop ().Dispose ();
			}*/
			
			foreach (IHistoryAction el in redoStack) {
				el.Dispose ();
			}

			redoStackUsage = 0;
			redoStack.Clear ();

			cleanupStacks ();
			IsChangedSinceSave = true;
		}

		public void undo () {
			if (undoStack.Count > 0) {
				IHistoryAction action = undoStack.First.Value;
				undoStack.RemoveFirst ();

				action.undo (image);
				redoStack.AddFirst (action);

				redoStackUsage += action.cost;
				undoStackUsage -= action.cost;

				cleanupStacks ();
				IsChangedSinceSave = true;
			}
		}

		public void redo () {
			if (redoStack.Count > 0 && !LockRedo) {
				IHistoryAction action = redoStack.First.Value;
				redoStack.RemoveFirst ();

				action.redo (image);
				undoStack.AddFirst (action);

				redoStackUsage -= action.cost;
				undoStackUsage += action.cost;

				cleanupStacks ();
				IsChangedSinceSave = true;
			}
		}
	}
}
