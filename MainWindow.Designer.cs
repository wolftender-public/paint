﻿
namespace paint {
	partial class MainWindow {
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent () {
			this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
			this.menuItemFile = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFileNew = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFileLoad = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.menuFileSave = new System.Windows.Forms.ToolStripMenuItem();
			this.menuFileSaveAs = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
			this.menuItemEdit = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditUndo = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditRedo = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.menuEditCopy = new System.Windows.Forms.ToolStripMenuItem();
			this.menuEditPaste = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.menuEditProperties = new System.Windows.Forms.ToolStripMenuItem();
			this.menuItemTools = new System.Windows.Forms.ToolStripMenuItem();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.toolStripFiller = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripMousePos = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStripImage = new System.Windows.Forms.ToolStripStatusLabel();
			this.toolStrip = new System.Windows.Forms.ToolStrip();
			this.panelCanvas = new System.Windows.Forms.FlowLayoutPanel();
			this.canvas = new System.Windows.Forms.PictureBox();
			this.panelTool = new System.Windows.Forms.FlowLayoutPanel();
			this.panelColorPicker = new System.Windows.Forms.FlowLayoutPanel();
			this.menuEditCut = new System.Windows.Forms.ToolStripMenuItem();
			this.mainMenuStrip.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.panelCanvas.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.canvas)).BeginInit();
			this.panelTool.SuspendLayout();
			this.SuspendLayout();
			// 
			// mainMenuStrip
			// 
			this.mainMenuStrip.BackColor = System.Drawing.SystemColors.Control;
			this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemFile,
            this.menuItemEdit,
            this.menuItemTools});
			this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
			this.mainMenuStrip.Name = "mainMenuStrip";
			this.mainMenuStrip.Size = new System.Drawing.Size(936, 24);
			this.mainMenuStrip.TabIndex = 0;
			this.mainMenuStrip.Text = "menuStrip1";
			// 
			// menuItemFile
			// 
			this.menuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNew,
            this.menuFileLoad,
            this.toolStripSeparator1,
            this.menuFileSave,
            this.menuFileSaveAs,
            this.toolStripSeparator2,
            this.menuFileExit});
			this.menuItemFile.Name = "menuItemFile";
			this.menuItemFile.Size = new System.Drawing.Size(37, 20);
			this.menuItemFile.Text = "File";
			// 
			// menuFileNew
			// 
			this.menuFileNew.Name = "menuFileNew";
			this.menuFileNew.Size = new System.Drawing.Size(142, 22);
			this.menuFileNew.Text = "New Image";
			// 
			// menuFileLoad
			// 
			this.menuFileLoad.Name = "menuFileLoad";
			this.menuFileLoad.Size = new System.Drawing.Size(142, 22);
			this.menuFileLoad.Text = "Load Image";
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
			// 
			// menuFileSave
			// 
			this.menuFileSave.Name = "menuFileSave";
			this.menuFileSave.Size = new System.Drawing.Size(142, 22);
			this.menuFileSave.Text = "Save";
			// 
			// menuFileSaveAs
			// 
			this.menuFileSaveAs.Name = "menuFileSaveAs";
			this.menuFileSaveAs.Size = new System.Drawing.Size(142, 22);
			this.menuFileSaveAs.Text = "Save As...";
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(139, 6);
			// 
			// menuFileExit
			// 
			this.menuFileExit.Name = "menuFileExit";
			this.menuFileExit.Size = new System.Drawing.Size(142, 22);
			this.menuFileExit.Text = "Exit Program";
			// 
			// menuItemEdit
			// 
			this.menuItemEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEditUndo,
            this.menuEditRedo,
            this.toolStripSeparator3,
            this.menuEditCopy,
            this.menuEditCut,
            this.menuEditPaste,
            this.toolStripSeparator6,
            this.menuEditProperties});
			this.menuItemEdit.Name = "menuItemEdit";
			this.menuItemEdit.Size = new System.Drawing.Size(39, 20);
			this.menuItemEdit.Text = "Edit";
			// 
			// menuEditUndo
			// 
			this.menuEditUndo.Name = "menuEditUndo";
			this.menuEditUndo.Size = new System.Drawing.Size(127, 22);
			this.menuEditUndo.Text = "Undo";
			// 
			// menuEditRedo
			// 
			this.menuEditRedo.Name = "menuEditRedo";
			this.menuEditRedo.Size = new System.Drawing.Size(127, 22);
			this.menuEditRedo.Text = "Redo";
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(124, 6);
			// 
			// menuEditCopy
			// 
			this.menuEditCopy.Name = "menuEditCopy";
			this.menuEditCopy.Size = new System.Drawing.Size(127, 22);
			this.menuEditCopy.Text = "Copy";
			// 
			// menuEditPaste
			// 
			this.menuEditPaste.Name = "menuEditPaste";
			this.menuEditPaste.Size = new System.Drawing.Size(127, 22);
			this.menuEditPaste.Text = "Paste";
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(124, 6);
			// 
			// menuEditProperties
			// 
			this.menuEditProperties.Name = "menuEditProperties";
			this.menuEditProperties.Size = new System.Drawing.Size(127, 22);
			this.menuEditProperties.Text = "Properties";
			// 
			// menuItemTools
			// 
			this.menuItemTools.Name = "menuItemTools";
			this.menuItemTools.Size = new System.Drawing.Size(46, 20);
			this.menuItemTools.Text = "Tools";
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripFiller,
            this.toolStripMousePos,
            this.toolStripImage});
			this.statusStrip.Location = new System.Drawing.Point(0, 653);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(936, 24);
			this.statusStrip.TabIndex = 1;
			this.statusStrip.Text = "statusStrip";
			// 
			// toolStripFiller
			// 
			this.toolStripFiller.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.toolStripFiller.Name = "toolStripFiller";
			this.toolStripFiller.Size = new System.Drawing.Size(775, 19);
			this.toolStripFiller.Spring = true;
			// 
			// toolStripMousePos
			// 
			this.toolStripMousePos.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
			this.toolStripMousePos.Name = "toolStripMousePos";
			this.toolStripMousePos.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
			this.toolStripMousePos.Size = new System.Drawing.Size(72, 19);
			this.toolStripMousePos.Text = "X: 0 Y: 0";
			// 
			// toolStripImage
			// 
			this.toolStripImage.Name = "toolStripImage";
			this.toolStripImage.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
			this.toolStripImage.Size = new System.Drawing.Size(74, 19);
			this.toolStripImage.Text = "W: 0 H: 0";
			// 
			// toolStrip
			// 
			this.toolStrip.AutoSize = false;
			this.toolStrip.BackColor = System.Drawing.SystemColors.Control;
			this.toolStrip.Dock = System.Windows.Forms.DockStyle.Left;
			this.toolStrip.ImageScalingSize = new System.Drawing.Size(26, 26);
			this.toolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
			this.toolStrip.Location = new System.Drawing.Point(0, 24);
			this.toolStrip.Name = "toolStrip";
			this.toolStrip.Padding = new System.Windows.Forms.Padding(2, 0, 1, 0);
			this.toolStrip.Size = new System.Drawing.Size(36, 629);
			this.toolStrip.TabIndex = 2;
			this.toolStrip.Text = "toolStrip1";
			// 
			// panelCanvas
			// 
			this.panelCanvas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelCanvas.AutoScroll = true;
			this.panelCanvas.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.panelCanvas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelCanvas.Controls.Add(this.canvas);
			this.panelCanvas.Location = new System.Drawing.Point(39, 99);
			this.panelCanvas.Name = "panelCanvas";
			this.panelCanvas.Size = new System.Drawing.Size(897, 554);
			this.panelCanvas.TabIndex = 3;
			// 
			// canvas
			// 
			this.canvas.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.canvas.Location = new System.Drawing.Point(3, 3);
			this.canvas.Name = "canvas";
			this.canvas.Size = new System.Drawing.Size(769, 390);
			this.canvas.TabIndex = 0;
			this.canvas.TabStop = false;
			this.canvas.Paint += new System.Windows.Forms.PaintEventHandler(this.canvas_Paint);
			this.canvas.MouseClick += new System.Windows.Forms.MouseEventHandler(this.canvas_Click);
			this.canvas.MouseDown += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseDown);
			this.canvas.MouseMove += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseMove);
			this.canvas.MouseUp += new System.Windows.Forms.MouseEventHandler(this.canvas_MouseUp);
			// 
			// panelTool
			// 
			this.panelTool.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panelTool.Controls.Add(this.panelColorPicker);
			this.panelTool.Location = new System.Drawing.Point(39, 24);
			this.panelTool.Margin = new System.Windows.Forms.Padding(0);
			this.panelTool.Name = "panelTool";
			this.panelTool.Size = new System.Drawing.Size(897, 72);
			this.panelTool.TabIndex = 4;
			// 
			// panelColorPicker
			// 
			this.panelColorPicker.Location = new System.Drawing.Point(0, 0);
			this.panelColorPicker.Margin = new System.Windows.Forms.Padding(0);
			this.panelColorPicker.Name = "panelColorPicker";
			this.panelColorPicker.Size = new System.Drawing.Size(455, 72);
			this.panelColorPicker.TabIndex = 0;
			// 
			// menuEditCut
			// 
			this.menuEditCut.Name = "menuEditCut";
			this.menuEditCut.Size = new System.Drawing.Size(127, 22);
			this.menuEditCut.Text = "Cut";
			// 
			// MainWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(936, 677);
			this.Controls.Add(this.panelTool);
			this.Controls.Add(this.panelCanvas);
			this.Controls.Add(this.toolStrip);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.mainMenuStrip);
			this.MainMenuStrip = this.mainMenuStrip;
			this.MinimumSize = new System.Drawing.Size(800, 600);
			this.Name = "MainWindow";
			this.Text = "Paint";
			this.mainMenuStrip.ResumeLayout(false);
			this.mainMenuStrip.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.panelCanvas.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.canvas)).EndInit();
			this.panelTool.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.MenuStrip mainMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem customizeToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem menuItemFile;
		private System.Windows.Forms.ToolStripMenuItem menuItemEdit;
		private System.Windows.Forms.ToolStripMenuItem menuItemTools;
		private System.Windows.Forms.ToolStripMenuItem menuFileNew;
		private System.Windows.Forms.ToolStripMenuItem menuFileLoad;
		private System.Windows.Forms.ToolStripMenuItem menuFileSave;
		private System.Windows.Forms.ToolStripMenuItem menuFileSaveAs;
		private System.Windows.Forms.ToolStripMenuItem menuFileExit;
		private System.Windows.Forms.ToolStripMenuItem menuEditUndo;
		private System.Windows.Forms.ToolStripMenuItem menuEditRedo;
		private System.Windows.Forms.ToolStripMenuItem menuEditProperties;
		private System.Windows.Forms.StatusStrip statusStrip;
		private System.Windows.Forms.ToolStripStatusLabel toolStripFiller;
		private System.Windows.Forms.ToolStripStatusLabel toolStripMousePos;
		private System.Windows.Forms.ToolStripStatusLabel toolStripImage;
		private System.Windows.Forms.ToolStrip toolStrip;
		private System.Windows.Forms.FlowLayoutPanel panelCanvas;
		private System.Windows.Forms.FlowLayoutPanel panelTool;
		private System.Windows.Forms.PictureBox canvas;
		private System.Windows.Forms.FlowLayoutPanel panelColorPicker;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripMenuItem menuEditCopy;
		private System.Windows.Forms.ToolStripMenuItem menuEditPaste;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		private System.Windows.Forms.ToolStripMenuItem menuEditCut;
	}
}

