﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace paint {
	public partial class MainWindow : Form {
		private bool isMouseDown = false;
		private Point lastMousePosition;
		private List<Tool> tools = new List<Tool> ();
		private Tool currentTool = null;
		private HistoryManager historyManager;
		private ColorPicker picker;
		private SelectTool selectTool;
		private string currentFile = null;
		private Timer clipboardTimer;

		public string CurrentFile {
			get { return currentFile; }
			private set {
				currentFile = value;

				if (currentFile != null) {
					Text = string.Format ("Paint - {0}", currentFile);
				} else {
					Text = "Paint";
				}
			}
		}

		public HistoryManager HistoryManager {
			get { return historyManager; }
		}

		public Color PrimaryColor {
			get { return picker.PrimaryColor; }
		}

		public Color SecondaryColor {
			get { return picker.SecondaryColor; }
		}

		// clipcoard and selection management
		private Bitmap bitmapClipboard = null;
		private Rectangle selectionRect, clipboardPosition;
		private bool selectionActive = false;

		private Rectangle getMaxBounds (in Rectangle a, in Rectangle b) {
			int minx = Math.Min (a.X, b.X);
			int miny = Math.Min (a.Y, b.Y);
			int maxx = Math.Max (a.X + a.Width, b.X + b.Width);
			int maxy = Math.Max (a.Y + a.Height, b.Y + b.Height);

			return new Rectangle (minx, miny, maxx - minx, maxy - miny);
		}

		public Rectangle ClipboardPosition {
			get { return clipboardPosition; }
			set {
				Rectangle inv = getMaxBounds (clipboardPosition, value);

				clipboardPosition = value;
				canvas.Invalidate (new Rectangle (inv.X - 3, inv.Y - 3, inv.Width + 6, inv.Height + 6));
			} 
		}

		public Rectangle Selection {
			get { return selectionRect; }
			set {
				if (canvas.Image != null) {
					Rectangle valueModified = Rectangle.Intersect (
						value, new Rectangle (0, 0, canvas.Image.Width, canvas.Image.Height)
					);

					Rectangle inv = getMaxBounds (valueModified, selectionRect);

					selectionRect = valueModified;
					selectionActive = true;

					canvas.Invalidate (new Rectangle (inv.X - 3, inv.Y - 3, inv.Width + 6, inv.Height + 6));
				} else {
					selectionRect = new Rectangle (0, 0, 0, 0);
					selectionActive = false;
				}
			}
		}

		public Bitmap BitmapClipboard {
			get { return bitmapClipboard; }
			set {
				bitmapClipboard = value;
				updateClipboardOptions ();
			}
		}

		private void drawDottedFrame (Graphics context, Color c1, Color c2, Rectangle rect) {
			using (Pen pen1 = new Pen (c1, 1)) {
				context.DrawRectangle (pen1, rect);
			}

			using (Pen pen2 = new Pen (c2, 1)) {
				pen2.DashPattern = new float [] { 5, 5 };
				context.DrawRectangle (pen2, rect);
			}
		}

		private void canvas_Paint (object sender, PaintEventArgs e) {
			Graphics context = e.Graphics;

			// render the clipboard
			if (BitmapClipboard != null) {
				context.DrawImage (BitmapClipboard, ClipboardPosition);
				drawDottedFrame (context, Color.Blue, Color.White, ClipboardPosition);
			}

			// render the selection rectangle
			if (selectionActive) {
				drawDottedFrame (context, Color.Black, Color.White, Selection);
			}
		}

		public void ResetSelection () {
			Selection = new Rectangle (0, 0, 0, 0);
			selectionActive = false;
		}

		public void ApplyClipboard () {
			if (BitmapClipboard != null && canvas.Image is Bitmap) {
				Bitmap bitmap = canvas.Image as Bitmap;
				Graphics context = Graphics.FromImage (bitmap);

				context.DrawImage (BitmapClipboard, ClipboardPosition);

				BitmapClipboard = null;
				ClipboardPosition = new Rectangle (0, 0, 0, 0);
			}
		}

		// end of clipboard and selection management

		private void undo_Click (object sender, EventArgs e) {
			if (currentTool != null && currentTool.OnUndo (canvas)) { // check if current tools currently allows performing undo
				historyManager.undo ();
				canvas.Invalidate ();

				menuEditRedo.Enabled = (historyManager.RedoStackSize > 0);
				menuEditUndo.Enabled = (historyManager.UndoStackSize > 0);
			}
		}

		private void redo_Click (object sender, EventArgs e) {
			historyManager.redo ();
			canvas.Invalidate ();

			menuEditRedo.Enabled = (historyManager.RedoStackSize > 0);
			menuEditUndo.Enabled = (historyManager.UndoStackSize > 0);
		}

		private void refreshSave () {
			menuFileSave.Enabled = (CurrentFile != null);
		}

		private bool closeProjectPrompt () {
			if (historyManager.IsChangedSinceSave) {
				if (MessageBox.Show ("Are you sure that you want to close this project? All changes will be lost!", "Close Project?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
					return true;
				} else {
					return false;
				}
			}

			return true;
		}

		private void exit_Click (object sender, EventArgs e) {
			Close ();
		}

		private void new_Click (object sender, EventArgs e) {
			if (!closeProjectPrompt ()) return;
			NewProject dialog = new NewProject (640, 480, true);
			
			if (dialog.ShowDialog () == DialogResult.OK) {
				CurrentFile = null;
				createEmtpyProject (dialog.ProjectWidth, dialog.ProjectHeight);
			}
		}

		private void load_Click (object sender, EventArgs e) {
			if (!closeProjectPrompt ()) return;

			using (OpenFileDialog dialog = new OpenFileDialog ()) {
				dialog.Filter = "PNG (*.png)|*.png|JPG (*.jpg, *.jpeg)|*.jpg;*.jpeg|BMP (*.bmp)|*.bmp";

				if (dialog.ShowDialog () == DialogResult.OK) {
					string filename = dialog.FileName;

					using (Image newImage = Image.FromFile (filename)) {
						Bitmap newBitmap = new Bitmap (newImage.Width, newImage.Height);
						using (Graphics ctx = Graphics.FromImage (newBitmap)) {
							using (Brush brBackground = new SolidBrush (picker.SecondaryColor)) {
								ctx.FillRectangle (brBackground, new Rectangle (0, 0, newBitmap.Width, newBitmap.Height));
							}

							ctx.DrawImageUnscaled (newImage, new Point (0, 0));

							canvas.Image = newBitmap;
							canvas.Invalidate ();

							historyManager = new HistoryManager (canvas.Image);
							CurrentFile = filename;
						}
					}
				}
			}
		}

		private void save_Click (object sender, EventArgs e) {
			if (CurrentFile != null) {
				canvas.Image.Save (CurrentFile);
				historyManager.IsChangedSinceSave = false;
			}
		}

		private void saveas_Click (object sender, EventArgs e) {
			using (SaveFileDialog dialog = new SaveFileDialog ()) {
				dialog.Filter = "PNG (*.png)|*.png|JPG (*.jpg, *.jpeg)|*.jpg;*.jpeg|BMP (*.bmp)|*.bmp";
				dialog.DefaultExt = ".png";

				if (dialog.ShowDialog () == DialogResult.OK) {
					CurrentFile = dialog.FileName;
					canvas.Image.Save (CurrentFile);
				}

				historyManager.IsChangedSinceSave = false;
				refreshSave ();
			}
		}

		// copy/cut/paste code
		// its a little bit hacky to make it work with selection tool
		// but the result is quite nice so i'll just leave it like that
		private void updateClipboardOptions () {
			bool val = (BitmapClipboard != null);

			menuEditCopy.Enabled = val;
			menuEditCut.Enabled = val;

			menuEditPaste.Enabled = Clipboard.ContainsImage ();
		}

		private void copy_Click (object sender, EventArgs e) {
			if (BitmapClipboard != null) {
				Clipboard.SetImage (BitmapClipboard);
				updateClipboardOptions ();
			}
		}
		
		private void cut_Click (object sender, EventArgs e) {
			if (BitmapClipboard != null) {
				if (selectTool != null) selectTool.OnCut (canvas);

				Clipboard.SetImage (BitmapClipboard);
				updateClipboardOptions ();

				// remove clipboard (cut)
				BitmapClipboard = null;
				ClipboardPosition = new Rectangle (0, 0, 0, 0);
			}
		}

		private void paste_Click (object sender, EventArgs e) {
			if (Clipboard.ContainsImage ()) {
				if (currentTool != selectTool) { // current tool is not select tool, switch to select tool
					currentTool = selectTool;
					foreach (ToolStripItem item in toolStrip.Items) {
						if (item is ToolStripButton) {
							ToolStripButton button = item as ToolStripButton;
							button.Checked = (tools [(int) button.Tag] == currentTool);
						}
					}
				}

				if (selectTool != null) selectTool.OnPaste (canvas);

				using (Image pastedImage = Clipboard.GetImage ()) {
					BitmapClipboard = new Bitmap (pastedImage.Width, pastedImage.Height);
					ClipboardPosition = new Rectangle (0, 0, pastedImage.Width, pastedImage.Height);

					using (Graphics ctx = Graphics.FromImage (BitmapClipboard)) {
						ctx.DrawImageUnscaled (pastedImage, new Point (0, 0));
					}
				}
			}
 		}

		private void RegisterMenuEvents () {
			// Edit menu
			menuEditUndo.Click += undo_Click;
			menuEditUndo.ShortcutKeys = Keys.Control | Keys.Z;

			menuEditRedo.Click += redo_Click;
			menuEditRedo.ShortcutKeys = Keys.Control | Keys.Y;

			menuEditPaste.Click += paste_Click;
			menuEditPaste.ShortcutKeys = Keys.Control | Keys.V;

			menuEditCut.Click += cut_Click;
			menuEditCut.ShortcutKeys = Keys.Control | Keys.X;

			menuEditCopy.Click += copy_Click;
			menuEditCopy.ShortcutKeys = Keys.Control | Keys.C;

			menuEditCopy.Enabled = menuEditCut.Enabled = false;
			menuEditRedo.Enabled = menuEditUndo.Enabled = false;

			menuEditPaste.Enabled = Clipboard.ContainsImage ();

			// File menu
			menuFileExit.Click += exit_Click;
			menuFileNew.Click += new_Click;
			menuFileLoad.Click += load_Click;
			menuFileSave.Click += save_Click;
			menuFileSaveAs.Click += saveas_Click;

			menuFileSave.ShortcutKeys = Keys.Control | Keys.S;
			menuFileNew.ShortcutKeys = Keys.Control | Keys.N;
			//menuFileSaveAs.ShortcutKeys = Keys.Shift | Keys.S;
			menuFileLoad.ShortcutKeys = Keys.Control | Keys.O;

			menuFileSave.Enabled = false;
		}

		// this is a "hack" that allows for easy checking for image in clipboard
		// instead of adding a lot of code for clipboard checking this is done
		// its cheap but it should work
		private void clipboardTimer_Tick (Object myObject, EventArgs myEventArgs) {
			menuEditPaste.Enabled = Clipboard.ContainsImage ();
		}

		public MainWindow () {
			InitializeComponent ();
			RegisterMenuEvents ();

			Selection = new Rectangle (0, 0, 0, 0);

			// initialize tools
			selectTool = new SelectTool (this);

			registerTool (selectTool);
			registerTool (new BrushTool (this));
			registerTool (new FillTool (this));

			createEmtpyProject (1600, 1023);
			DoubleBuffered = true;

			// automatically check first tool
			foreach (ToolStripItem item in toolStrip.Items) {
				if (item is ToolStripButton) {
					ToolStripButton button = item as ToolStripButton;
					button.Checked = true;
					currentTool = tools [(int) button.Tag];
					currentTool.OnSelected (panelTool);
					break;
				}
			}

			// color picker ui
			picker = new ColorPicker ();
			picker.Width = panelColorPicker.Width;
			panelColorPicker.Controls.Add (picker);

			// clipboard timer
			clipboardTimer = new Timer ();
			clipboardTimer.Tick += clipboardTimer_Tick;
			clipboardTimer.Interval = 700;
			clipboardTimer.Start ();
		}

		public void registerTool (Tool tool) {
			ToolStripButton menuItem = new ToolStripButton (null, tool.Icon, toolstrip_Click);
			menuItem.Tag = tools.Count;
			menuItem.ToolTipText = tool.Name;
			menuItem.CheckOnClick = true;

			tools.Add (tool);
			toolStrip.ShowItemToolTips = true;
			toolStrip.Items.Add (menuItem);
		}

		private void createEmtpyProject (int width, int height) {
			Bitmap bmp = new Bitmap (width, height);
			
			// todo: change this to unsafe code
			for (int i = 0; i < width * height; ++i) {
				int x = i % width, y = i / width;
				bmp.SetPixel (x, y, Color.White);
			}

			canvas.Image = bmp;

			canvas.Width = width;
			canvas.Height = height;

			canvas.Invalidate ();

			historyManager = new HistoryManager (canvas.Image);
		}

		private void toolstrip_Click (object sender, EventArgs e) {
			ToolStripButton senderButton = sender as ToolStripButton;

			if (senderButton != null && senderButton.GetCurrentParent () == toolStrip) {
				foreach (ToolStripItem item in toolStrip.Items) {
					if (!(item is ToolStripButton)) continue;
					ToolStripButton button = item as ToolStripButton;

					if (button.Tag != senderButton.Tag) button.Checked = false;
					else button.Checked = true;
				}

				// handle tool selection
				int toolId = (int) senderButton.Tag;
				if (currentTool != null) currentTool.OnDeselected (panelTool);
				currentTool = tools [toolId];
				currentTool.OnSelected (panelTool);
			}
		}

		private void canvas_MouseDown (object sender, MouseEventArgs e) {
			isMouseDown = true;
			lastMousePosition = e.Location;

			if (currentTool != null) currentTool.OnMouseDown (e, canvas, e.Location);
		}

		private void canvas_MouseUp (object sender, MouseEventArgs e) {
			isMouseDown = false;

			if (currentTool != null) currentTool.OnMouseUp (e, canvas, e.Location);

			menuEditRedo.Enabled = (historyManager.RedoStackSize > 0);
			menuEditUndo.Enabled = (historyManager.UndoStackSize > 0);
		}

		private void canvas_MouseMove (object sender, MouseEventArgs e) {
			Point newMousePosition = e.Location;
			if (currentTool != null) currentTool.OnMouseMove (e, canvas, lastMousePosition, newMousePosition, isMouseDown);

			lastMousePosition = newMousePosition;
		}

		private void canvas_Click (object sender, MouseEventArgs e) {
			if (currentTool != null) currentTool.OnMouseClick (e, canvas, e.Location);
		}
	}
}
