﻿
namespace paint {
	partial class NewProject {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose (bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose ();
			}
			base.Dispose (disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent () {
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkKeepRatio = new System.Windows.Forms.CheckBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.numericHeight = new System.Windows.Forms.NumericUpDown();
			this.numericWidth = new System.Windows.Forms.NumericUpDown();
			this.buttonCreate = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericWidth)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkKeepRatio);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.numericHeight);
			this.groupBox1.Controls.Add(this.numericWidth);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(218, 141);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Project Settings";
			// 
			// checkKeepRatio
			// 
			this.checkKeepRatio.AutoSize = true;
			this.checkKeepRatio.Location = new System.Drawing.Point(66, 102);
			this.checkKeepRatio.Name = "checkKeepRatio";
			this.checkKeepRatio.Size = new System.Drawing.Size(121, 19);
			this.checkKeepRatio.TabIndex = 5;
			this.checkKeepRatio.Text = "Keep Aspect Ratio";
			this.checkKeepRatio.UseVisualStyleBackColor = true;
			this.checkKeepRatio.Click += new System.EventHandler(this.checkKeepRatio_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 70);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(43, 15);
			this.label2.TabIndex = 3;
			this.label2.Text = "Height";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 34);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(39, 15);
			this.label1.TabIndex = 2;
			this.label1.Text = "Width";
			// 
			// numericHeight
			// 
			this.numericHeight.Location = new System.Drawing.Point(66, 66);
			this.numericHeight.Name = "numericHeight";
			this.numericHeight.Size = new System.Drawing.Size(120, 23);
			this.numericHeight.TabIndex = 1;
			this.numericHeight.ValueChanged += new System.EventHandler(this.numericHeight_ValueChanged);
			// 
			// numericWidth
			// 
			this.numericWidth.Location = new System.Drawing.Point(66, 30);
			this.numericWidth.Name = "numericWidth";
			this.numericWidth.Size = new System.Drawing.Size(120, 23);
			this.numericWidth.TabIndex = 0;
			this.numericWidth.ValueChanged += new System.EventHandler(this.numericWidth_ValueChanged);
			// 
			// buttonCreate
			// 
			this.buttonCreate.Location = new System.Drawing.Point(157, 166);
			this.buttonCreate.Name = "buttonCreate";
			this.buttonCreate.Size = new System.Drawing.Size(75, 23);
			this.buttonCreate.TabIndex = 1;
			this.buttonCreate.Text = "Create";
			this.buttonCreate.UseVisualStyleBackColor = true;
			this.buttonCreate.Click += new System.EventHandler(this.buttonCreate_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.Location = new System.Drawing.Point(74, 166);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 2;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			// 
			// NewProject
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(244, 200);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonCreate);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "NewProject";
			this.Text = "Create New Project";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericWidth)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numericHeight;
		private System.Windows.Forms.NumericUpDown numericWidth;
		private System.Windows.Forms.Button buttonCreate;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.CheckBox checkKeepRatio;
	}
}