﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace paint {
	public partial class NewProject : Form {
		public const int maxWidth = 2048;
		public const int maxHeight = 2048;

		private int width, height;
		private double aspectRatio;

		private bool adjustValues = true;

		public int ProjectWidth {
			get { return width; }
		}

		public int ProjectHeight {
			get { return height; }
		}

		public NewProject (int initialWidth, int initialHeight, bool keepRatio) {
			InitializeComponent ();

			AcceptButton = buttonCreate;
			CancelButton = buttonCancel;

			MinimizeBox = MaximizeBox = false;

			numericWidth.Maximum = maxWidth;
			numericWidth.Minimum = 8;

			numericHeight.Maximum = maxHeight;
			numericHeight.Minimum = 8;

			numericHeight.Increment = 1;
			numericHeight.Increment = 1;

			numericWidth.Value = width = Math.Min (initialWidth, maxWidth);
			numericHeight.Value = height = Math.Min (initialHeight, maxHeight);

			aspectRatio = (double) width / height;
			checkKeepRatio.Checked = keepRatio;
		}

		private void buttonCreate_Click (object sender, EventArgs e) {
			DialogResult = DialogResult.OK;
		}

		private void checkKeepRatio_Click (object sender, EventArgs e) {
			aspectRatio = (double) width / height; // save aspect ratio
		}

		private void numericWidth_ValueChanged (object sender, EventArgs e) {
			if (checkKeepRatio.Checked && adjustValues) {
				width = (int) numericWidth.Value;

				adjustValues = false;
				numericHeight.Value = height = (int) Math.Floor (width / aspectRatio);
				adjustValues = true;
			}
		}

		private void numericHeight_ValueChanged (object sender, EventArgs e) {
			if (checkKeepRatio.Checked && adjustValues) {
				height = (int) numericHeight.Value;

				adjustValues = false;
				numericWidth.Value = width = (int) Math.Floor (height * aspectRatio);
				adjustValues = true;
			}
		}
	}
}
