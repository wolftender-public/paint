﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace paint {
	public class SelectTool : ChunkTool {
		private Image icon;

		public override string Name => "Selection";
		public override Image Icon => icon;

		private Point selectionOrigin;
		private bool clipboardDrag = false;

		public SelectTool (MainWindow window) : base (window) {
			icon = Properties.Resources.SelectIcon;
		}

		private void markChunks (Rectangle rect) {
			for (int cx = rect.X - chunkWidth; cx <= rect.X + rect.Width + chunkWidth; cx = cx + chunkWidth) {
				for (int cy = rect.Y - chunkWidth; cy <= rect.Y + rect.Height + chunkWidth; cy = cy + chunkWidth) {
					markChunk (cx, cy);
				}
			}
		}

		private void applyChanges () {
			if (IsDrawing) {
				markChunks (MainWindow.ClipboardPosition);
				MainWindow.ApplyClipboard ();
				endDrawing ();
			}
		}

		public void OnCut (PictureBox canvas) {
			if (IsDrawing) {
				endDrawing ();
			}
		}

		public void OnPaste (PictureBox canvas) {
			applyChanges ();
			MainWindow.ResetSelection ();
			clipboardDrag = true;
			startDrawing (canvas);
		}

		public override bool OnUndo (PictureBox canvas) {
			applyChanges ();
			return true;
		}

		public override void OnMouseDown (MouseEventArgs e, PictureBox canvas, Point position) {
			if (MainWindow.BitmapClipboard != null) {
				if (MainWindow.ClipboardPosition.IntersectsWith (new Rectangle (e.Location, new Size (1, 1)))) {
					clipboardDrag = true;
					return;
				} else {
					applyChanges ();
				}
			}

			MainWindow.Selection = new Rectangle (position.X, position.Y, 0, 0);
			selectionOrigin = position;

			base.OnMouseDown (e, canvas, position);
		}

		public override void OnMouseMove (MouseEventArgs e, PictureBox canvas, Point lastPosition, Point position, bool isMouseDown) {
			if (MainWindow.BitmapClipboard != null) {
				if (MainWindow.ClipboardPosition.IntersectsWith (new Rectangle (e.Location, new Size (1, 1)))) {
					Cursor.Current = Cursors.SizeAll;
				} else {
					Cursor.Current = Cursors.Default;
				}
			}

			if (isMouseDown) {
				if (clipboardDrag) {
					int dx = position.X - lastPosition.X;
					int dy = position.Y - lastPosition.Y;
					Rectangle cp = MainWindow.ClipboardPosition;

					MainWindow.ClipboardPosition = new Rectangle (cp.X + dx, cp.Y + dy, cp.Width, cp.Height);
				} else {
					int width = Math.Abs (position.X - selectionOrigin.X);
					int height = Math.Abs (position.Y - selectionOrigin.Y);

					MainWindow.Selection = new Rectangle (Math.Min (position.X, selectionOrigin.X), Math.Min (position.Y, selectionOrigin.Y), width, height);
					base.OnMouseMove (e, canvas, lastPosition, position, isMouseDown);
				}
			}
		}

		public override void OnMouseUp (MouseEventArgs e, PictureBox canvas, Point position) {
			if (clipboardDrag) {
				clipboardDrag = false;
				return;
			}

			if (MainWindow.Selection.Width != 0 && MainWindow.Selection.Height != 0) {
				startDrawing (canvas);

				Bitmap source = canvas.Image as Bitmap;
				MainWindow.BitmapClipboard = source.Clone (MainWindow.Selection, source.PixelFormat);

				Graphics context = Graphics.FromImage (source);

				markChunks (MainWindow.Selection);
				context.FillRectangle (new SolidBrush (MainWindow.SecondaryColor), MainWindow.Selection);

				MainWindow.ClipboardPosition = MainWindow.Selection;
				MainWindow.ResetSelection ();
			}

			base.OnMouseUp (e, canvas, position);
		}

		public override void OnDeselected (FlowLayoutPanel toolPanel) {
			applyChanges ();
			base.OnDeselected (toolPanel);
		}
	}
}
