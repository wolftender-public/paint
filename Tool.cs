﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace paint {
	public abstract class Tool {
		public abstract string Name { get; }
		public abstract Image Icon { get; }

		private MainWindow mainWindow;

		protected MainWindow MainWindow {
			get { return mainWindow; }
		}

		public Tool (MainWindow window) {
			mainWindow = window;
		}

		public virtual void OnSelected (FlowLayoutPanel toolPanel) { }
		public virtual void OnDeselected (FlowLayoutPanel toolPanel) { }

		public virtual void OnMouseClick (MouseEventArgs e, PictureBox canvas, Point position) { }
		public virtual void OnMouseDown (MouseEventArgs e, PictureBox canvas, Point position) { }
		public virtual void OnMouseMove (MouseEventArgs e, PictureBox canvas, Point lastPosition, Point position, bool isMouseDown) { }
		public virtual void OnMouseUp (MouseEventArgs e, PictureBox canvas, Point position) { }
		public virtual bool OnUndo (PictureBox canvas) { return true; }
	}
}
